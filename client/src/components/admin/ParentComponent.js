import React, { useState } from 'react';
import { Button, Box } from '@mui/material';
import CategoryForm from './CategoryForm';

const ParentComponent = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const [editingCategory, setEditingCategory] = useState(null);

  const handleOpenModal = (category = null) => {
    setEditingCategory(category);
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
    setEditingCategory(null);
  };

  const handleSave = () => {
    // Your save logic here
    // e.g., fetch categories again or update the state
    handleCloseModal();
  };

  return (
    <Box>
      <Button variant="contained" onClick={() => handleOpenModal()}>
        Add Category
      </Button>

      {modalOpen && (
        <CategoryForm category={editingCategory} onClose={handleCloseModal} onSave={handleSave} />
      )}
    </Box>
  );
};

export default ParentComponent;
