// src/api/axios.js
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://ydayselectedbacken-458e79f51ef9.herokuapp.com/api',
});

export default axiosInstance;
